# Deepflame's Chat Cards

A module to alter the base chat cards. This will color the cards based on the player who added the chat entry.

## Issues

If you have any issues or concerns, please don't hesitate to open an issue on the tracker <link to tracker> or reach out to me on the Foundry discord server: Deepflame#0875.
